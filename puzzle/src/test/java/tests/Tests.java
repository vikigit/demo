package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.Duration;

import org.checkerframework.checker.units.qual.h;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Tests {
	public WebDriver driver = new ChromeDriver();

	@Before
	public void testSetUp() {

		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		driver.get("http://uitestingplayground.com/");
	}

	@After
	public void testTearDown() throws InterruptedException {
		Thread.sleep(5000);
		driver.quit();
	}

	@Test
	@Ignore
	public void dynamicId() {
		driver.findElement(By.xpath("//*[@href=\"/dynamicid\"]")).click();
		driver.findElement(By.cssSelector(".btn-primary")).click();
	}

	@Test
	@Ignore
	public void classAtribute() {
		driver.findElement(By.xpath("//*[@href=\"/classattr\"]")).click();
		driver.findElement(By.className("btn-warning")).click();
		driver.findElement(By.cssSelector(".btn-warning")).click();
	}

	@Test
	@Ignore
	public void hiddenLayers() {
		driver.findElement(By.xpath("//*[@href=\"/hiddenlayers\"]")).click();
		driver.findElement(By.id("greenButton")).click();
		driver.findElement(By.id("greenButton")).click();
	}

	@Test
	@Ignore
	public void loadDelay() {
		driver.findElement(By.xpath("//*[@href=\"/loaddelay\"]")).click();
		driver.findElement(By.cssSelector(".btn-primary")).isDisplayed();
	}

	@Test
	@Ignore
	public void ajaxRequest() throws InterruptedException {
		driver.findElement(By.xpath("//*[@href=\"/ajax\"]")).click();
		driver.findElement(By.cssSelector(".btn-primary")).click();
//		Thread.sleep(16000);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".bg-success")));
		driver.findElement(By.cssSelector(".bg-success")).isDisplayed();
	}

	@Test
	@Ignore
	public void clientSideDelay() {
		driver.findElement(By.xpath("//*[@href=\"/clientdelay\"]")).click();
		driver.findElement(By.cssSelector(".btn-primary")).click();
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".bg-success")));
		driver.findElement(By.cssSelector(".bg-success")).isDisplayed();
	}

	@Test
	@Ignore
	public void click() {
		driver.findElement(By.xpath("//*[@href=\"/click\"]")).click();
		driver.findElement(By.cssSelector(".btn-primary")).click();
	}

	@Test
	@Ignore
	public void textInput() {
		driver.findElement(By.xpath("//*[@href=\"/textinput\"]")).click();
		driver.findElement(By.id("newButtonName")).sendKeys("some test");
		driver.findElement(By.id("updatingButton")).click();
	}

	@Test
	@Ignore
	public void scrollBars() {
		driver.findElement(By.xpath("//*[@href=\"/scrollbars\"]")).click();
		WebElement element = driver.findElement(By.id("hidingButton"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}

	@Test
	@Ignore
	public void dynamicTable() {
		driver.findElement(By.xpath("//*[@href=\"/dynamictable\"]")).click();
		String actualResult = driver
				.findElement(By.xpath("//span[text()='Chrome']/parent::*/span[contains(text(),'%')]")).getText();
		String expectedResult = driver.findElement(By.cssSelector(".bg-warning")).getText();
		
		System.out.println(actualResult);
		System.out.println(expectedResult);
		assertTrue(expectedResult.contains(actualResult));
	}

	@Test
	//@Ignore
	public void verifyText() {
		driver.findElement(By.xpath("//*[@href=\"/verifytext\"]")).click();
		String actualResult = driver.findElement(By.xpath("(//span[contains(text(), 'Welcome')])[2]")).getText();
		System.out.println(actualResult);
		
	}

	@Test
	//@Ignore 
	public void progressBar() {
		driver.findElement(By.xpath("//*[@href=\"/progressbar\"]")).click();
		WebElement start = driver.findElement(By.xpath("//button[@id='startButton']"));
		start.click();

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@aria-valuenow='74']")));
		WebElement stop = driver.findElement(By.xpath("//button[@id='stopButton']"));
		stop.click();

	}

	@Test
	//@Ignore
	public void visibilityButtons() {
		driver.findElement(By.xpath("//*[@href=\"/visibility\"]")).click();
		WebElement hide = driver.findElement(By.xpath("//button[@onclick='HideButtons()']"));
		hide.click();

		// WebElement removed =
		// driver.findElement(By.xpath("//button[@id='removedButton']"));
		// WebElement zeroWidthButton =
		// driver.findElement(By.xpath("//button[@id='zeroWidthButton']"));
		// WebElement overlappedButton =
		// driver.findElement(By.xpath("//button[@id='overlappedButton']"));
		// WebElement transparentButton =
		// driver.findElement(By.xpath("//button[@id='transparentButton']"));
		// WebElement invisibleButton =
		// driver.findElement(By.xpath("//button[@id='invisibleButton']"));
		// WebElement notdisplayedButton =
		// driver.findElement(By.xpath("//button[@id='notdisplayedButton']"));
		// WebElement offscreenButton =
		// driver.findElement(By.xpath("//button[@id='offscreenButton']"));

	}

	@Test
	//@Ignore
	public void sampleApplication() {
		driver.findElement(By.xpath("//*[@href=\"/sampleapp\"]")).click();
		WebElement userName = driver.findElement(By.xpath("//input[@name='UserName']"));
		userName.sendKeys("Viki");
		WebElement password = driver.findElement(By.xpath("//input[@name='Password']"));
		password.sendKeys("pwd");

		WebElement loginButton = driver.findElement(By.xpath("//button[@id='login']"));
		loginButton.click();

		assertTrue(driver.findElement(By.xpath("//button[@id='login']")).isDisplayed());

	}

	@Test
	//@Ignore
	public void mouseOver() {
		driver.findElement(By.xpath("//*[@href=\"/mouseover\"]")).click();
		Actions act = new Actions(driver);
		WebElement clickMe = driver.findElement(By.xpath("//a[@title='Click me']"));
		act.doubleClick(clickMe).perform();

		WebElement count = driver.findElement(By.xpath("//span[@id='clickCount']"));
		String actualResult = count.getText().toString();
		String expResult = "2";
		assertEquals(expResult, actualResult);

	}

	@Test
	//@Ignore
	public void nonBreakingSpace() {
		driver.findElement(By.xpath("//*[@href=\"/nbsp\"]")).click();
		WebElement btn = driver.findElement(By.xpath("//h4[text()='Playground']/following-sibling::button"));
		btn.click();

	}

	@Test
	//@Ignore
	public void overlappedElement() {
		driver.findElement(By.xpath("//*[@href=\"/overlapped\"]")).click();

		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement name = driver.findElement(By.xpath("//input[@id='name']"));

		js.executeScript("arguments[0].scrollIntoView();", name);
		name.sendKeys("Viki");

	}

}
